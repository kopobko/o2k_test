<?

class ens_seo_titles extends CModule{

	var $MODULE_ID = "ens.seo_titles";
	var $MODULE_VERSION;
	var $MODULE_VERSION_DATE;
	var $MODULE_NAME;
	var $MODULE_DESCRIPTION;
	var $MODULE_CSS;

	function getEvents(){
		return [
			['main','OnBeforeEndBufferContent',$this->MODULE_ID,'\Ens\SeoTitles\Events\MainEvents','OnBeforeEndBufferContent'],
		];
	}
	function ens_seo_titles(){

		$arModuleVersion = [];
		$path = str_replace("\\","/",__FILE__);
		$path = substr($path,0,strlen($path) - strlen("/index.php"));
		include($path."/version.php");
		if(is_array($arModuleVersion) && array_key_exists("VERSION",$arModuleVersion)){
			$this->MODULE_VERSION = $arModuleVersion["VERSION"];
			$this->MODULE_VERSION_DATE = $arModuleVersion["VERSION_DATE"];
		}
		$this->MODULE_NAME = "[$this->MODULE_ID] Управление заголовками каталога";
		$this->MODULE_DESCRIPTION = "";
	}

	function InstallFiles(){
		return true;
	}

	function InstallEvents(){
		$events = $this->getEvents();
		if(!empty($events)){
			$eventManager = Bitrix\Main\EventManager::getInstance();

			foreach($events as $arEvent){
				list($module,$event,$toModule,$toClass,$toEvent) = $arEvent;
				$eventManager->registerEventHandlerCompatible($module,$event,$toModule,$toClass,$toEvent);
			}
		}
	}
	function UnInstallEvents(){
		$events = $this->getEvents();
		if(!empty($events)){
			$eventManager = Bitrix\Main\EventManager::getInstance();

			foreach($events as $arEvent){
				list($module,$event,$toModule,$toClass,$toEvent) = $arEvent;
				$eventManager->unRegisterEventHandler($module,$event,$toModule,$toClass,$toEvent);
			}
		}
	}

	function UnInstallFiles(){
		return true;
	}

	function DoInstall(){

		global $DOCUMENT_ROOT,$APPLICATION;

		$this->InstallFiles();
		$this->InstallEvents();
		$this->InstallDB();

		RegisterModule($this->MODULE_ID);
		$APPLICATION->IncludeAdminFile("Установка модуля ".$this->MODULE_ID,$DOCUMENT_ROOT."/local/modules/dv_module/install/step.php");
	}

	function DoUninstall(){

		global $DOCUMENT_ROOT,$APPLICATION;
		$this->UnInstallFiles();
		$this->UnInstallDB();
		$this->UnInstallEvents();

		UnRegisterModule($this->MODULE_ID);
		$APPLICATION->IncludeAdminFile("Деинсталляция модуля ".$this->MODULE_ID,$DOCUMENT_ROOT."/local/modules/dv_module/install/unstep.php");
	}
}

?>