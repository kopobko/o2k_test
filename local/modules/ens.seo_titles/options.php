<?
use Bitrix\Main\Loader;
use Bitrix\Main\Localization\Loc;
use Bitrix\Main\Config\Option;


$module_id = "ens.seo_titles";
$CAT_RIGHT = $APPLICATION->GetGroupRight($module_id);
if ($CAT_RIGHT>="R") :
	Loc::LoadMessages(__FILE__);


	if(!Loader::includeModule($module_id)){
		return false;
	}

	if(isset($_REQUEST['action']) && $_REQUEST['action']=='save'){
	    Option::set($module_id,'h1_template',trim($_REQUEST['h1_template']));
	    Option::set($module_id,'title_template',trim($_REQUEST['title_template']));
	}
	$strH1Template = Option::get($module_id,'h1_template');
	$strTitleTemplate = Option::get($module_id,'title_template');
	$aTabs = array(
		array("DIV" => "edit1", "TAB" => "Настройки", "ICON" => "", "TITLE" => 'Общие настройки модуля'),
	);
	$tabControl = new CAdminTabControl("tabControl", $aTabs);

	$tabControl->Begin();
	?>
	<form method="POST" action="" >
		<input type="hidden" name="action" value="save">
		<? echo bitrix_sessid_post();?>

		<?$tabControl->BeginNextTab();?>
		<tr class="heading">
			<td colspan="2">Настройки</td>
		</tr>
		<tr>
			<td><?=Loc::GetMessage('H1_TEMPLATE_TITLE')?></td>
			<td><input type="text" name="h1_template" value="<?=$strH1Template?>"></td>
		</tr>
		<tr>
			<td><?=Loc::GetMessage('TITLE_TEMPLATE_TITLE')?></td>
			<td><input type="text" name="title_template" value="<?=$strTitleTemplate?>"></td>
		</tr>
		<?$tabControl->Buttons();?>
		<input type="submit" name="save" value="<?echo GetMessage("MAIN_SAVE")?>">
		<?$tabControl->End();?>
	</form>
<?endif;?>