<?php

namespace Ens\SeoTitles\Events;


use Bitrix\Main\Config\Option;
use Ens\SeoTitles\Context;

class MainEvents{

	public function OnBeforeEndBufferContent(){
		if(defined('ENS_SEO_TITLES') && ENS_SEO_TITLES==true){

			global $APPLICATION;

			$h1 = Context::FormatPageTitle($APPLICATION->GetTitle(false));
			$title = Context::FormatMetaTitle($APPLICATION->GetPageProperty('title'));

			$APPLICATION->SetTitle($h1);
			$APPLICATION->SetPageProperty('title',$title);

		}
	}
}