<?
namespace Ens\SeoTitles;

use Bitrix\Main\Config\Option;

class Context{

	const MODULE_ID = 'ens.seo_titles';


	public static function FormatPageTitle($title = ""){
		$key = '#H1#';
		$template = trim(Option::get(self::MODULE_ID,'h1_template'));
		if($template=='')return $title;

		$title = strpos($template,$key)!==false?str_replace($key,$title,$template):$template;

		return $title;
	}

	public static function FormatMetaTitle($title = ""){
		$key = '#TITLE#';
		$template = trim(Option::get(self::MODULE_ID,'title_template'));
		if($template=='')return $title;
		$title = strpos($template,$key)!==false?str_replace($key,$title,$template):$template;

		return $title;
	}
}
?>