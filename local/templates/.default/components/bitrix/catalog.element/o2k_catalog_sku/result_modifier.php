<? if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true){
	die();
}

/**
 * @var CBitrixComponentTemplate $this
 * @var CatalogElementComponent  $component
 */
$component = $this->getComponent();
$arParams = $component->applyTemplateModifications();

$arAvailablesSkuProps = [];

$strDelimeter = '-';
$strRuleDelimeter = '-i-';
$selectedSku = $arParams['SKU_CODE'];

$arSkuPropsCodes = [];

$hasSeoHandler = \Bitrix\Main\Loader::includeModule('ens.seo_titles');

//Todo: Cache this
foreach($arResult['SKU_PROPS'] as $skuKey=>&$arSkuProperty){

	if(!isset($arResult['OFFERS_PROP'][$arSkuProperty['CODE']])){
		continue;
	}
	$arSkuPropsCodes[$arSkuProperty['ID']] = $arSkuProperty['CODE'];

	$arSkuProperty['SEO_PAREMETER'] = CUtil::translit($arSkuProperty['NAME'],'ru');
	foreach($arSkuProperty['VALUES'] as $valueKey=>&$arValue){
		$arValue['SEO_PAREMETER'] = CUtil::Translit($arValue['NAME'],'ru');
		$key = $skuKey.'_'.$valueKey;
		if($arValue['SEO_PAREMETER']!='' && $arSkuProperty['SEO_PAREMETER']!=''){

			$code = implode($strDelimeter,[$arSkuProperty['SEO_PAREMETER'],$arValue['SEO_PAREMETER'] ]);

			$arAvailablesSkuProps[$key] = $code;
			$arAvailablesSkuData[$key] = [
				'PROPERTY_ID' => $arSkuProperty['ID'],
				'VALUE'=>$arValue['ID'],
				'CODE'=>$code,
				'TITLE' =>implode(' ',[$arSkuProperty['NAME'],$arValue['NAME']]),
			];
		}
	}

	unset($arValue);

}
unset($arSkuProperty);
$originalMeta = $arResult["META_TAGS"];

if($hasSeoHandler){
	$arResult["META_TAGS"]['TITLE'] = \Ens\SeoTitles\Context::FormatPageTitle($originalMeta['TITLE']);
	$arResult["META_TAGS"]['BROWSER_TITLE'] = \Ens\SeoTitles\Context::FormatMetaTitle($originalMeta['BROWSER_TITLE']);
}

foreach($arResult['JS_OFFERS'] as $k=>&$arJsOffer){
	$arFullKey = [];
	$arFullTitle = [];

	foreach($arJsOffer['TREE'] as $propKey=>$propValue){
		$arKey = explode('_',$propKey);
		$propID = $arKey[1];
		$propCode = $arSkuPropsCodes[$propID];
		$key = $propCode.'_'.$propValue;
		$arSkuPropData = $arAvailablesSkuData[$key];
		$arFullKey[] = $arSkuPropData['CODE'];
		$arFullTitle[] = $arSkuPropData['TITLE'];
	}

	$ipropValues = (new \Bitrix\Iblock\InheritedProperty\ElementValues($arResult["OFFERS_IBLOCK"], $arJsOffer["ID"]))->getValues();
	$offerKey = implode($strRuleDelimeter,$arFullKey);

	$addTitle = implode(' ',$arFullTitle);
	$arJsOffer['SEO_KEY'] = $offerKey;

	$fullTitle = $fullBrowhertitle = '';

	if(empty($ipropValues['ELEMENT_PAGE_TITLE'])){
		$fullTitle = isset($originalMeta['TITLE'])?$originalMeta['TITLE']:$arResult['NAME'];
		$fullTitle.=' '.$addTitle;
	} else {
		$fullTitle = $ipropValues['ELEMENT_PAGE_TITLE'];
	}


	if(empty($ipropValues['ELEMENT_META_TITLE'])){
		$fullBrowhertitle = isset($originalMeta['BROWSER_TITLE'])?$originalMeta['BROWSER_TITLE']:$arResult['NAME'];
		$fullBrowhertitle.=' '.$addTitle;
	} else {
		$fullBrowhertitle = $ipropValues['ELEMENT_META_TITLE'];
	}
	if($hasSeoHandler){
		$fullTitle = \Ens\SeoTitles\Context::FormatPageTitle($fullTitle);
		$fullBrowhertitle = \Ens\SeoTitles\Context::FormatMetaTitle($fullBrowhertitle);
	}
	$arJsOffer['SEO_TITLE'] = $fullTitle;
	$arJsOffer['SEO_BROWSER_TITLE'] = $fullBrowhertitle;

	$arJsOffer['SEO_URL'] = $arResult['DETAIL_PAGE_URL'].$offerKey.'/';

	if($selectedSku !=='' && $selectedSku==$offerKey){

		$arResult['OFFERS_SELECTED'] = $k;

		$arResult["META_TAGS"]['TITLE'] = $arJsOffer['SEO_TITLE'];
		$arResult["META_TAGS"]['BROWSER_TITLE'] = $arJsOffer['SEO_BROWSER_TITLE'];
	}
}
unset($arJsOffer);

